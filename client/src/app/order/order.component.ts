import { Component, OnInit } from '@angular/core';
import { OrderService } from './order.service';
import { IOrder } from '../shared/models/order';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  orders: IOrder[];

  constructor(private orderService: OrderService) { }

  ngOnInit(): void {
    this.getOrders();
  }

  getOrders(){
    this.orderService.getOrderForUser().subscribe((orders: IOrder[]) =>{
      this.orders = orders;
      console.log(orders);
    }, error => {
      console.log(error);
    })
  }

}
